FROM voidlinux/voidlinux
MAINTAINER https://bitbucket.org/bluesquall/docker-matplotlib/issues

RUN xbps-install -Sy git curl python3-matplotlib
